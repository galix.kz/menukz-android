package kz.compass.group.menukz.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "codeplayon.com";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setRestaurantVersion(int restaurantId, int version){
        editor.putInt("restaurant_version"+restaurantId+"", version);
        editor.commit();
    }
    public int getRestaurantVersion(int restaurantId){
        return pref.getInt("restaurant_version"+restaurantId,-1);
    }
    public void setRestaurantDownloaded(int restaurantId, boolean restaurantDownloaded ){
        editor.putBoolean("restaurant_downloaded"+restaurantId+"", restaurantDownloaded);
        editor.commit();
    }
    public boolean isRestaurantDownloaded(int restaurantId){
        return pref.getBoolean("restaurant_downloaded"+restaurantId+"",false);
    }
}