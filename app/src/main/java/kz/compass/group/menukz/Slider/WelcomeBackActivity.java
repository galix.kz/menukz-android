package kz.compass.group.menukz.Slider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import kz.compass.group.menukz.QR.QRScanner;
import kz.compass.group.menukz.R;


public class WelcomeBackActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_back);
    }
    public void start(View v){
        startActivity(new Intent(WelcomeBackActivity.this, QRScanner.class));
    }
}
